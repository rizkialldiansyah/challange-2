package GenerateFile;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class Cluster extends processingData {

    @Override
    protected HashMap<Integer, Integer> Clustering(ArrayList<Integer> listValues, int num, String opr) {
        Set<Integer> hSet = new HashSet<Integer>();
        hSet = convertToSet(listValues);
        HashMap<Integer, Integer> valuesPerclass = new HashMap<Integer, Integer>();

        if (opr.equals("kurang")) {
            for (int x : hSet) {
                if (x >= 1 && x < num) {
                    int count = 0;
                    for (int y : listValues) {
                        if (y == x) {
                            count += 1;
                        }
                    }
                    valuesPerclass.put(x, count);
                }
            }
        } else {
            for (int x : hSet) {
                if (x >= num && x <= 10) {
                    int count = 0;
                    for (int y : listValues) {
                        if (y == x) {
                            count += 1;
                        }
                    }
                    valuesPerclass.put(x, count);
                }
            }

        }

        return valuesPerclass;
    }


    public String generateData(ArrayList<Integer> listValues, int num, String opr) {
        HashMap<Integer, Integer> hmap = Clustering(listValues, num, opr);
        String statement;
        if ((hmap == null || hmap.isEmpty())) {
            statement = "--------------------------------------\n";
            statement += "HASIL PENGELOMPOKAN NILAI " + opr.toUpperCase() + " DARI \'" + num + "\' TIDAK DITEMUKAN!!\n";
            statement += "--------------------------------------\n";
        } else {
            statement = "--------------------------------------\n";
            statement += "HASIL PENGELOMPOKAN NILAI " + opr.toUpperCase() + " DARI \'" + num + "\'\n";
            statement += "--------------------------------------\n";
            for (Integer key : hmap.keySet()) {
                statement += key + " = " + hmap.get(key) + "\n";
            }
            statement += "--------------------------------------\n";
        }
        return statement.toString();
    }


    @Override
    protected void writeTxt(String filePath, String statement) {
        try {
            File file = new File(filePath);
            file.exists();
            if (file.createNewFile()) {
                System.out.println("New file has been created!");
            } else {
                System.out.println("Rewriting file in " + filePath);
            }
            FileWriter writer = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(writer);
            bw.write(statement);
            bw.newLine();
            bw.flush();
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Set<Integer> convertToSet(ArrayList<Integer> aList) {
        Set<Integer> hSet = new HashSet<Integer>();

        for (int x : aList) {
            hSet.add(x);
        }

        return hSet;
    }
}
