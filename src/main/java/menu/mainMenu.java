package menu;
import GenerateFile.*;
import Data.*;

import java.util.*;

public class mainMenu {
    static Scanner inp = new Scanner(System.in);
    static CSVReader readerCSV = new CSVReader();
    static Cluster clustering = new Cluster();
    static Statistics countStatistics = new Statistics();
    static Scanner scan;


    public mainMenu(String path) {
        readerCSV.readCSV(path);

    }


    public static ArrayList<Integer> showAllClass(ArrayList<String> classList) {
        int num = 1;
        System.out.println("--------------------------------------");
        System.out.println("PILIH KELAS");
        System.out.println("--------------------------------------");
        for (String kelas : classList) {
            System.out.println(num + ". " + kelas + "");
            num += 1;
        }
        System.out.println("--------------------------------------");
        int chooseClass = Integer.parseInt(inp.nextLine());


        return readerCSV.getValuesbyClass(classList.get(chooseClass - 1));
    }

    // Output Menu
    public static void showMainMenu() {
        System.out.println("--------------------------------------");
        System.out.println("MENU UTAMA");
        System.out.println("--------------------------------------");
        System.out.println("1. Generate txt untuk menampilkan Median, Mean, atau Modus");
        System.out.println("2. Generate txt untuk menampilkan kelompok data dengan operator kurang dari atau lebih dari");
        System.out.println("0. Exit");
        System.out.println("--------------------------------------");
    }

    public static ArrayList<Integer> showOpsiNilai() {
        System.out.println("--------------------------------------");
        System.out.println("PILIH NILAI YANG AKAN DIGENERATE");
        System.out.println("--------------------------------------");
        System.out.println("1. Nilai dari semua kelas");
        System.out.println("2. Nilai dari pilihan kelas tertentu");
        System.out.println("0. Kembali ke-Main Menu");
        System.out.println("--------------------------------------");
        int chooseOpsiNilai = Integer.parseInt(inp.nextLine());

        switch (chooseOpsiNilai) {
            case 1:
                return readerCSV.getValues();
            case 2:
                return showAllClass(readerCSV.getKelas());
            case 0:
                GenerateAPP(scan);
                return null;
            default:
                System.out.println("Angka yang diinputkan salah!");
                return showOpsiNilai();

        }
    }


    public static String showMenuCluster(int nilaiInput) {
        String opr;
        System.out.println("--------------------------------------");
        System.out.println("PILIH OPSI PENGELOMPOKAN");
        System.out.println("--------------------------------------");
        System.out.println("Nilai yang anda inputkan: " + nilaiInput);
        System.out.println("1. Data dikelompokan kurang dari nilai input");
        System.out.println("2. Data dikelompokan lebih dari nilai input");
        System.out.println("0. Kembali ke-Main Menu");
        System.out.println("--------------------------------------");
        int chooseCluster = Integer.parseInt(inp.nextLine());

        switch (chooseCluster) {
            case 1: {
                opr = "kurang";
                return opr;
            }
            case 2: {
                opr = "lebih";
                return opr;
            }
            case 0: {
                GenerateAPP(scan);
                return null;
            }
            default: {
                System.out.println("Anda salah menginputkan opsi!");

                return showMenuCluster(nilaiInput);
            }
        }
    }

    public static int showInputCluster() {

        System.out.println("--------------------------------------");
        System.out.println("Input diantara 1-10");
        System.out.println("--------------------------------------");
        int num = Integer.parseInt(inp.nextLine());

        if (num <= 1 || num >= 10) {
            System.out.println("Anda menginputkan angka selain 1-10\n");
            return showInputCluster();
        } else {
            return num;
        }
    }


    // Start App
    public static void GenerateAPP(Scanner scan) {
        ArrayList<Integer> dataList = new ArrayList<Integer>();
        String goStart, filePath;
        try{
            showMainMenu();
            int chooseMainMenu = Integer.parseInt(inp.nextLine());

            switch (chooseMainMenu) {
                case 1: {
                    dataList = showOpsiNilai();
                    System.out.println(countStatistics.generateData(dataList));

                    System.out.println("Input File Path: ");
                    filePath = inp.nextLine();
                    countStatistics.writeTxt(filePath, countStatistics.generateData(dataList));

                    System.out.println("Data sudah digenerate!");
                    System.out.println("--------------------------------------\n");

                    System.out.println("Klik ENTER untuk Kembali ke Main Menu");
                    goStart = inp.nextLine();

                    GenerateAPP(scan);
                    break;
                }
                case 2: {
                    int clusterNum = showInputCluster();
                    String opr = showMenuCluster(clusterNum);
                    dataList = showOpsiNilai();
                    System.out.println(clustering.generateData(dataList, clusterNum, opr));

                    System.out.println("Input File Path: ");
                    filePath = inp.nextLine();
                    countStatistics.writeTxt(filePath, clustering.generateData(dataList, clusterNum, opr));

                    System.out.println("Data sudah digenerate!");
                    System.out.println("--------------------------------------\n");

                    System.out.println("Klik ENTER untuk Kembali ke Main Menu");
                    goStart = inp.nextLine();

                    GenerateAPP(scan);
                    break;

                }
                case 0: {
                    System.out.println("TERIMAKASIH!");
                    break;
                }
                default: {
                    System.out.println("PILIHANMU TIDAK ADA!");
                    GenerateAPP(scan);
                }
            }
        }catch (NumberFormatException e){
            System.out.println("ERROR!, Anda bukan memasukan angka ");
        }
    }
}
