package Data;

import java.util.*;

public abstract class dataSekolah {

    protected HashMap<String, ArrayList<Integer>> valuesPerclass;
    protected ArrayList<Integer> values;
    protected ArrayList<String> kelas;

    public dataSekolah() {
        values = new ArrayList<Integer>();
        kelas = new ArrayList<String>();
        valuesPerclass = new HashMap<String, ArrayList<Integer>>();
    }



    public void addValues(int value) {
        this.values.add(value);
    }

    public void addKelas(String kelas) {
        this.kelas.add(kelas);
    }

    public void addValuesPerclass(String kelas, ArrayList<Integer> values) {
        this.valuesPerclass.put(kelas, values);
    }

    public abstract ArrayList<Integer> getValues();

    public abstract ArrayList<String> getKelas();

    public abstract ArrayList<Integer> getValuesbyClass(String kelas);

}
