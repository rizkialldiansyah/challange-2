package exceptions;

import Data.CSVReader;
import menu.mainMenu;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Scanner;

public class testTryCatch {
    mainMenu MainMenu;
    CSVReader CsvReader;
    Scanner scan = new Scanner(System.in);


    @Test
    void readCSV_rightPath(){
        String path = "C:/Data/ALDI/Course/Binar X Synergy/data/data_sekolah.csv";
        CsvReader = new CSVReader();
        CsvReader.readCSV(path);
    }

    @Test
    void readCSV_worngPath(){
        String path = "C:/worngPath";
        CsvReader = new CSVReader();
        CsvReader.readCSV(path);
    }

    @Test
    void readCSV_NullInput(){
        String path = null;
        CsvReader = new CSVReader();
        CsvReader.readCSV(path);
    }



    @Test
    void menu_ExitInput(){
        String input = "0";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
        mainMenu.GenerateAPP(new Scanner(input));
    }

    @Test
    void menu_ErrorInput(){
        String input = "null";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
        mainMenu.GenerateAPP(new Scanner(input));
    }


    @Test
    void menu_showMenu(){
        mainMenu.showMainMenu();
    }
}
